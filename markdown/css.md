# Programmation CSS


### Introduction

- Le World Wide Web a introduit les feuilles de styles en cascade pour compléter la langage HTML
	- = **Cascading Style Sheets**
- Les CSS permettent de gérer l'apparence des documents
- Les feuilles indiquent aux balises HTML leur comportement ou **style**

<div style="width:10%;  display: block;  margin: auto;">![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/CSS3_logo_and_wordmark.svg/340px-CSS3_logo_and_wordmark.svg.png)</div>


### Les styles

- Un style rassemble tous les attributs que l'on peut appliquer à des types de textes similaires
	- Attribut : taille,format
	- Textes similaires : titres, en-têtes, pieds de page
- Les styles  donnent un nom commun à des groupes d'attributs


<iframe src="https://support.office.com/en-ie/article/customize-or-create-new-styles-in-word-d38d6e47-f6fc-48eb-a607-1eb120dec563" class='code' style="height:600px; width:100%" scrolling="yes"> </iframe>


## Règle d'écriture
![](fig/css.png)


### Structure des feuilles de styles

- Une feuille de styles est composée d'un ensemble de descriptions de styles
```
Sélecteurs{
	propriété_1 : valeurs_1;
	propriété_2 : valeurs_2;
}
```
- Exemple :
```
H2{
	color: navy;
	font : 18px;
	font-family: sans-serif;
}
```


### Sélecteurs regroupés

- Possibilité de regrouper plusieurs sélecteurs pour une même description de style
- Exemple :
```
	H1, H2, H3, H4 {color: blue}
```
est identique à :
```
	H1 {color: blue}
	H2 {color: blue}
	H3 {color: blue}
	H4 {color: blue} 
```


### Localisation des styles

- La déclaration de  règles de styles peut être soit :
	1. **externe** au document HTML dans une feuille de style
	+ **interne** au document HTML dans la section **`HEAD`**


### Feuille de styles externe

- Séparation de la **présentation** de la page HTML,
- **Réduit la taille** de la page HTML,
- Style identique pour l’ensemble d’un site,
- **Évolution** rapide du «design» d’un site.
- Feuille de style **spécifique au média** (taille de l’écran, imprimante,...)


## Feuille de styles externe

- Une feuille de styles externe est un fichier texte portant habituellement l'extension **.css**
- Le lien entre le document HTML et le fichier CSS s’effectue dans la section **`<HEAD>`** d’un document HTML
- Exemple : 
```
<HEAD>
	<TITLE>Histoire des feuilles de styles</TITLE>
	<link REL="StyleSheet" TYPE="text/css" HREF="../styles.css">
</HEAD>
```


### Explication
```
<HEAD>
	<TITLE>Histoire des feuilles de styles</TITLE>
	<link REL="StyleSheet" TYPE="text/css" HREF="../styles.css">
</HEAD>
```
- La balise **`<LINK>`** avertit le navigateur qu'il doit établir un lien 
- **`rel=stylesheet`** précise qu'il s'agit d'une feuille externe 
- **`type="text/css"`** avertit qu'il s'agit de feuilles de style en cascade 
- **`href=" ... "`** définit l'emplacement de la feuille de style 	


<div style="display: inline-grid;grid-template-columns: 500px 500px 200px;">
	<div>
		HTML
<code><pre>
```
<!DOCTYPE html>
<HTML lang="fr">
	<HEAD>
		<title>Le css</title>
		<LINK
			REL="stylesheet" 
			TYPE="text/css"
			HREF="style1.css">		
		<meta charset="utf-8">	
	</HEAD>
	<BODY>
		<H1> Un titre </H1>
		<H2> Un autre titre </H2>
		<p> Un joli paragraphe</p>
	</BODY>
</HTML>	
```
</pre>
</code>	</div> 
	<div>
		CSS
<code><pre>
```
body {
	color: red ;
}
h2 { 
	text-align:right;
}
h1 {
	font-family:"Courier";
} 	
p {
	border-style:solid;
}
```
</pre></code>
	</div> 
</div> 

<iframe src="exemples/exemple0.html"  class='code' style='resize: both;overflow: auto;height:150px; width: 100%'> </iframe>


## Feuille de styles interne

- Une feuille de styles interne est insérée en en-tête du fichier HTML à l'aide de l'élément **`<STYLE>`**
- Ces styles seront définis uniquement pour le document courant


### Exemple
<div style="display: inline-grid;grid-template-columns: 500px 500px 200px;">
	<div>
<code><pre>
```
<!DOCTYPE html>
<HTML lang="fr">
	<HEAD>
		<title>Le css</title>
		<style>
			body {
				color: red ;
			}
			h2 { 
				text-align:right;
			}
			h1 {
				font-family:"Courier";
			} 	
			p {
				border-style:solid;
			}
		</style>
		<meta charset="utf-8">	
	</HEAD>
	<BODY>
		<H1> Un titre </H1>
		<H2> Un autre titre </H2>
		<p> Un joli paragraphe</p>
	</BODY>
</HTML>	
```
</pre>
</code>	</div> 
	<div>
<iframe src="exemples/exemple0.html"  class='code' style='resize: both;overflow: auto;height:500px; width: 300px'> </iframe>
	</div> 
</div> 


## Les styles

<iframe src="https://www.tutorialrepublic.com/css-reference/css3-properties.php" style='height:600px'>


## Les classes

- Doit commencer par un "**.**"
- Définit des propriétés pour plusieurs sélecteurs
- Est appelée avec "**`class=`**" en paramètre de balise
- Exemple
	- Dans le css
```
.monStyle { 
  background-color: yellow;
  text-align: center;
}
```
	- Dans le html
```
<p class="monStyle"> Un texte </p>
```


### Exemple
<div style="display: inline-grid;grid-template-columns: 500px 500px 200px;">
	<div>
		HTML
<code><pre>
```
<H1 class="joli"> Un titre </H1>
<H2> Un <b class="joli">autre</b> titre </H2>
<p> Un joli <b>paragraphe</b></p>
<p class="joli"> Un joli paragraphe</p>	
<ul>
	<li>Toto</li>
	<li class="joli">Tata</li>
	<li>Titi</li>
</ul>	
```
</pre>
</code>	</div> 
	<div>
		CSS
<code><pre>
```
.joli { 
  color: red;
  font-family: Gill Sans Extrabold, sans-serif;
  text-decoration: underline;
}
```
</pre></code>
	</div> 
</div> 

<iframe src="exemples/exemple1.html"  class='code' style='resize: both;overflow: auto;height:225px; width: 100%'> </iframe>


## Les ID

- Doit commencer par un "**#**"
- Définit des propriétés d'un élément **unique**
- Est appelé avec "**`id=`**" en paramètre de balise
- Exemple
	- Dans le css
```
#uniqueStyle { 
	   background-color: yellow;
	   text-align: center;
}
```
	- Dans le html
```
<p id="monStyle"> Un texte </p>
```


### Exemple
<div style="display: inline-grid;grid-template-columns: 500px 500px 200px;">
	<div>
		HTML
<code><pre>
```
<H1> Un titre </H1>
<H2> Un <b>autre</b> titre </H2>
<p> Un joli <b>paragraphe</b></p>
<p> Un joli paragraphe</p>	
<ul>
	<li>Toto</li>
	<li id="tresJoli">Tata</li>
	<li>Titi</li>
</ul>	
```
</pre>
</code>	</div> 
	<div>
		CSS
<code><pre>
```
h1{	
  text-decoration: underline; }
b{
  color: red; }
.joli { 
  font-weight: bold;
  font-family: 'Helvetica Neue', sans-serif;
  color: blue;
}
```
</pre></code>
	</div> 
</div> 

<iframe src="exemples/exemple2.html"  class='code' style='resize: both;overflow: auto;height:225px; width: 100%'> </iframe>


## La balise div
- La balise **`<div>`** est un bloc
- C'est un objet rectangulaire
- Peut être répartie sur plusieurs lignes
- Possède les attributs **`margin`**, **`padding`**, **`width`**, **`height`**
- **Souvent définie par une classe**
```
<div class="xxx">
	<H1> ... </H1>
	<H2> ... </H2>
	<p> ... </p>
	<p> ... </p>	
	<p> ... </p>			
</div>
```


### Exemple
<div style="display: inline-grid;grid-template-columns: 500px 500px 200px;">
	<div>
		HTML
<code><pre>
```
<div class="partie1">
	<p> Un joli <b>paragraphe</b></p>
	<p> Un joli paragraphe</p>
</div>	
<div class="partie2">
	<ul>
		<li>Toto</li>
		<li>Tata</li>
	</ul>	
</div>
```
</pre>
</code>	</div> 
	<div>
		CSS
<code><pre>
```
.partie1{	
  border:4px solid red;
  border-radius: 10px;
  margin:15px;
}
.partie2 { 
  border:4px dotted black;
  padding:5px;
}
```
</pre></code>
	</div> 
</div> 

<iframe src="exemples/exemple3.html"  class='code' style='resize: both;overflow: auto;height:175px; width: 100%'> </iframe>


## La balise span
- La balise **`<span>`** est inline
- S'inscrit dans le flux du contenu
- Pas de hauteur ni de largeur ni de marge
- Placée à l'intérieur d'un paragraphe
- **Souvent définie par une classe**

```
<p><span class="xxx">0000000<span class="xxx">111111</span>
222222</span></p>
```


### Exemple
<div style="display: inline-grid;grid-template-columns: 550px 450px 200px;">
	<div>
		HTML
<code><pre>
```
<p> 
  La balise <span class="joli1">span
  </span> est <span class="joli2">inline
  </span>, elle s'inscrit dans le <span 
  class="joli2">flux du contenu</span>, 
  peut être répartie sur <span class=
  "joli2">plusieurs lignes</span>. 
</p>
```
</pre></code>	
	</div> 
	<div>
		CSS
<code><pre>
```
.joli1{	
  color: red;
  font-weight: bold;
}
.joli2 { 
  color: blue;
  font-style: italic;
}
```
</pre></code>
	</div> 
</div> 

<iframe src="exemples/exemple4.html"  class='code' style='resize: both;overflow: auto;height:175px; width: 100%'> </iframe>


## Validation W3C
- La validation CSS peut être réalisée <a href="http://jigsaw.w3.org/css-validator/"  target="_blank">ici</a>
- La validation HTML peut être réalisée <a href="https://validator.w3.org"  target="_blank">ici</a>

<div style="display: inline-grid;grid-template-columns: 500px 500px 200px;">
	<div>
		![](fig/fig7.png)
	</div>
	<div>
		![](fig/fig8.png)
	</div>
</div>


## Les marges

![](fig/fig2.png)
- margin = marge externe 
- padding = marge interne

	- ***Remarque*** : Modifier la marge interne (**padding**) a pour conséquence d’agrandir la boîte concernée, en revanche modifier la marge externe (**margin**) n’a pas d’incidence sur les dimensions de la boîte


### Comment tester son style
- En utilisant le **Lorem Ipsum**
- Un faux texte utilisé en imprimerie
- Standard depuis les années **1500**
- Vient de "De Finibus Bonorum et Malorum" de **Cicéron**

![](fig/fig3.png)   |   ![](fig/fig4.png)
--- | ---


## Lorem Ipsum
<div style="height:500px; 
	background: #FFFFFF;
	border:4px #000000 solid; 
	border-radius: 10px;
	box-shadow: 5px 10px;
	overflow:scroll;">
	<pre style="white-space: pre-wrap"><code>
<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non lacus pretium, mattis enim efficitur, tempus ex. Nam facilisis in orci nec tincidunt. Ut molestie id nibh eu imperdiet. Proin tellus ex, vestibulum nec lorem quis, viverra sollicitudin justo. Mauris vitae enim dapibus, vulputate massa eget, facilisis elit. Nullam dapibus porta ultrices. Nam pellentesque ligula nec orci consectetur imperdiet. Nullam vehicula dapibus mi. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>

<p>Etiam et tortor gravida, maximus ex eu, convallis tortor. Ut scelerisque enim vel metus mattis aliquam. Pellentesque dictum laoreet velit, vel pulvinar ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus sit amet interdum massa. Donec sagittis ipsum ante, et molestie arcu finibus non. Mauris facilisis at eros vitae vestibulum. Duis ultricies et ex sit amet sagittis.</p>

<p>Integer interdum, nibh a tristique porta, nisl turpis faucibus mauris, in lobortis enim ligula at tellus. Sed a lacus massa. Morbi dapibus, tellus eu fringilla ultrices, tortor mauris blandit erat, at fermentum diam leo at lorem. Pellentesque egestas risus et ex imperdiet, commodo pulvinar lectus sollicitudin. Nulla sit amet vulputate velit. Vivamus ac eros tincidunt, rhoncus justo at, volutpat dui. Vivamus a bibendum metus. Nunc vulputate ipsum in arcu accumsan, ut rhoncus ante venenatis. Aenean sem ex, ullamcorper a eleifend vitae, scelerisque non sapien. Ut porttitor cursus leo, pulvinar aliquet augue euismod vel. Aenean fermentum auctor tortor, rutrum varius purus venenatis et. Ut condimentum commodo libero at tristique. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec quam dolor, ultrices eu ex in, eleifend imperdiet purus. Proin aliquam bibendum ante, ut consectetur justo tincidunt et.</p>

<p>Etiam in sem eu lorem dictum ultrices a non erat. Nullam vitae tincidunt massa. Cras in ex mauris. Mauris pharetra risus ac metus cursus, at vulputate odio scelerisque. Vivamus eu tristique dui. In dolor orci, lacinia non lectus vel, luctus sollicitudin nunc. Quisque tortor arcu, iaculis sit amet egestas nec, rutrum porttitor nulla. Sed mattis gravida sem, sit amet sollicitudin nunc malesuada scelerisque. Nullam rhoncus metus eu lacus pretium gravida. Nulla vestibulum sapien sem, vel ultricies neque pulvinar vel. Curabitur tincidunt lobortis nunc, et viverra nisl laoreet in.</p>

<p>Suspendisse ultricies ornare imperdiet. Nullam semper rhoncus ultrices. Phasellus mollis molestie lorem. Praesent volutpat ligula id urna luctus congue. Quisque in posuere velit, eu pellentesque massa. Vivamus convallis, erat dignissim porttitor gravida, odio orci feugiat massa, ut tincidunt lectus est ac mauris. Aliquam bibendum, dolor vitae commodo porta, risus purus ultrices urna, in cursus elit magna at magna.</p>
</code></pre>
</div>


### Exemple

<iframe src="exemples/exemple5.html"  class='code' style='resize: both;overflow: auto;height:500px; width: 100%'> </iframe>


<div style="display: inline-grid;grid-template-columns: 400px 440px 200px;">
	<div>
![](fig/fig5.png)
	</div>
	<div>
		***Aide :***
		<ul>
<li>margin: <span style="color:blue">50px</span> <span style="color:green">100px</span> ;
<li>padding-right: <span style="color:green">200px</span> ;
<li>text-align: right ;
		</ul>	
	</div>
</div>

![](fig/fig6.png)


## Gestion des boites
- **`float`** Pour positionner automatiquement des éléments 
	- &rarr; **`left`** ou **`right`**) 
- **`margin: 0 auto;`** pour centrer un élément
- **`div`** avec une classe permet de donner les propriétés

```
.boites{
	border-radius: 10px;
	float:left;	
	background-color: #FFAAAA;
	border:4px solid red	
}
```


### Positionnement automatique
<iframe src="exemples/boite1.html" style="width: 100%; height: 500px;"></iframe>

- &rarr; Dépend de la **taille** des boîtes et de la page web


### Exemple avec balises de structure
![](fig/fig9.png)


### Petites améliorations :
- Autre police
- Couleur de fond
- un peu de padding
- Bords arrondi

<div style="display: inline-grid;grid-template-columns: 500px 500px 200px;">
	<div>
		<iframe src="exemples/exemple6.html" style="width: 100%; height: 500px;"></iframe>
	</div>
	<div>
		<code><pre style="white-space: pre-wrap">
```
font-family: 'Trebuchet MS', Arial, sans-serif;	
border-radius: 10px 40px 10px 40px; 	border:1px solid black;
padding: 5px;
box-shadow: 20px 20px black;
```
		</pre></pre>
	</div>
</div>


### Quelques nouveautés avec CSS3
- Coins arrondis
- Ombrage
- Gradient
- Images de fond multiples
- Mise en page multi-colonnes
- Transformation
- Transition

&rarr; Cf <a target="_blank" href="https://medium.com/beginners-guide-to-mobile-web-development/whats-new-in-css-3-dcd7fa6122e1">Documentation</a>


## Les boites flexibles
- Les boîtes doivent être dans un contenant (**`body`** ou un **`div`**)
- Ce contenant doit avoir 
	- **`display: flex;`**
	- Une <a target="_blank" href="https://www.w3schools.com/css/css3_flexbox.asp">propriété</a> d'affichage
- Les éléments dans le contenant peuvent aussi avoir des <a target="_blank" href="https://www.w3schools.com/css/css3_flexbox.asp">propriétés</a>
- On peut avoir des boîtes flex dans d'autres boîtes flex
<div style="display: inline-grid;grid-template-columns: 500px 500px 200px;">
	<div>
		<code><pre>
			```
.flex-container {
  display: flex;
  height: 300px;
  justify-content: center;
  align-items: center;
}
```
		</pre></code>
	</div>
	<div>
		<code><pre>
			```
.flex-element {
	order: 1;
}		
```
		</pre></code>
	</div>		 


### Exemple
<iframe src="exemples/boite2.html" style="width: 100%; height: 500px;"></iframe>	


## L’affichage en grille
- Les boîtes doivent être dans un contenant (**`body`** ou un **`div`**)
- Ce contenant doit avoir 
	- **`display: grid;`**
	- Une <a target="_blank" href="https://www.w3schools.com/css/css_grid.asp">propriété</a> d'affichage
- Les éléments dans le contenant peuvent aussi avoir des <a target="_blank" href="https://www.w3schools.com/css/css_grid.asp">propriétés</a>

<div style="display: inline-grid;grid-template-columns: 500px 500px 200px;">
	<div>
		<code><pre>
			```
.grid-container {
  display: grid;
  grid-column-gap: 50px;
  grid-template-columns: 200px 200px 200px;
}
```
		</pre></code>
	</div>
	<div>
		<code><pre>
			```
.grid-element {
  grid-row-start: 1;
  grid-row-end: 3;
}		
```
		</pre></code>
	</div>		 


### Exemple
<iframe src="exemples/boite3.html" style="width: 100%; height: 500px;"></iframe>	