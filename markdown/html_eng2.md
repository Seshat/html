# HTML Programming


### Browser processing

- Appropriate softwares:
	- Notepad (**notepad++**, **sublime Text**, <a href="http://brackets.io" target="_blank">brackets</a>, ...)
	- Web Browser (**firefox**, **chrome**, **safari**, ...)

---
<div style="display: inline-grid;grid-template-columns: 500px 200px 200px;">
	<div>
		 Notepad
		<code><pre>
			```
Hello world, how are you? 
Goodbye!!!!!
```
		</pre></code>
	</div>	
	<div>
		Browser
	<iframe src="exemples/bonjour.html" style='height:175px; width: 100%'> </iframe>
	</div>
</div>


### Layout

- The layout is made by a computer language!
- It's HTML

<div style="width:50%;display: block;  margin: auto;">![](https://www.w3.org/html/logo/img/html5-topper.png)</div>


### HTML programming

- The **HTML** language (Hyper Text Markup Language) goes hand-in-hand with the **HTTP** protocol (Hyper Text Transport Protocol) which allows to convey documents on the web
- HTML is the **universal language** on the universal network which is Internet


### HTML programming
- An HTML file (extension **.htm** or **.html**) **=**  **text** file
- The creation of HTML documents is at reach of everyone who knows how to use a simple text file editor
- HTML is only a command package using intelligible beacons, (example : ```<title>```) 


### (small) History
- Before : Internet = gigantic **Server network**
- 1990's  : Tim Berners-Lee created the **web**
- HTML 5: **2014**

<iframe src="https://en.wikipedia.org/wiki/HTML" style='height:600px; width: 100%;'> </iframe>


### HTML5 principle

- **Philosophy**  : Content separation, structure and display
- **Content** = Everything related to the user's knowledge using the natural language, images, sounds, and animations 
- **Structure** = Logic organisation of the content using elements such as `<body>`, `<h1>`, `<p>`, etc. 
- **Presentation** = How does the content manifest for the user using an output device, such as a display screen, a printer, a vocal browser, a Braille console, etc. 


### HTML5 Additional idea

- Avoid «**Ctrl+C**», «**Ctrl+V**» !!!
 - No layout, only the content and the structure
 - Header must be known by heart
- Examples :
```
HTML 4.01 Strict
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" 
	"http://www.w3.org/TR/html4/strict.dtd">
HTML 4.01 Transitional
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">
HTML 4.01 Frameset
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN"
 "http://www.w3.org/TR/html4/frameset.dtd">
```


### The structure of the document must be specified

- HTML5 defines a new content management template
- Dedicated beacon : `<header>`, `<article>`, `<nav>` or `<footer>`
- Objective : Affect a common style (CSS)
<iframe src="https://guide.freecodecamp.org/html/html5-semantic-elements/" style='height:800px; width: 100%;'> </iframe>


### Structure of an HTML document 
- **`<HTML>`**
	- Every HTML document starts with the <HTML> beacon which reports that the following document is coded in HTML
	- Example : `<HTML> document ... </HTML>`
- **`<HEAD>`**
	- The beacon`<HEAD>` defines the header of the document
	- Few other beacons are inserted 
- **`<BODY>`**
	- Defines the core of the document (text, beacon, images, ...)
- **`<TITLE>`**
	- Unique title which must be explicit since it is used as a bookmark from the navigation interfaces


## The structure

Example :
```
<!DOCTYPE html>
<HTML lang="fr">
	<HEAD>
		<TITLE> My first HTML document </TITLE>
	</HEAD>
	<BODY>
   		... Text of the document ...
	</BODY>
</HTML>
```


### Result

```
<!DOCTYPE html>
<HTML lang="fr">
	<HEAD>
		<TITLE> My first HTML document </TITLE>
	</HEAD>
	<BODY>
   		Hello world, how are you?
		Goodbye!!!!!
	</BODY>
</HTML>
```

<iframe src="exemples/bonjour.html"  class='code' style='height:200px; width: 100%'> </iframe>


<iframe src="https://codesandbox.io/embed/1y1mz1o09j?fontsize=14&view=editor" title="Ex1" style="width:100%; height:600px; border:0; border-radius: 4px; overflow:hidden;" sandbox="allow-modals allow-forms allow-popups allow-scripts allow-same-origin"></iframe>
<iframe src="https://codesandbox.io/embed/1y1mz1o09j?fontsize=14" title="ex1" style="width:100%; height:500px; border:0; border-radius: 4px; overflow:hidden;" sandbox="allow-modals allow-forms allow-popups allow-scripts allow-same-origin"></iframe>


## Titles and paragraphs

- There are 6 title levels to split a document

- The digit states the heading level, from **`H1`** to **`H6`**
	- Example : **`<H1> Big title (level 1)</H1`**>
    - or : **`<H6> Small title (level 6) </H6>`**

- Every paragraph starts with an opening beacon **`<P>`** and ends with a closing beacon **`</P>`**

- Insertion of comments inside a document is coded as follows:
**`<!-- This is a comment -->`**


```
<!DOCTYPE html>
<HTML lang="fr">
	<HEAD>
		<TITLE> HTML document </TITLE>
	</HEAD>
	<BODY>
		<h1>Courtesy<h1>
		<P>Hellow world, how are you?</P>
		<P>Goodbye!!!!!</P>
	</BODY>
</HTML>
```

<iframe src="exemples/bonjour3.html"  class='code'  style='height:200px; width: 100%;'> </iframe>


## Lists

- There is 4 major kinds of lists
	- Numbered list (**`<OL>`**)
	- Bulleted list ( **`<UL>`**)
	- Glossary list
	- Directory list
- Every list is bounded by 
	- **`<UL>`** and **`</UL>`** or **`<DL>`** and **`</DL>`**
- Every element of the list has its own beacons
	- **`<LI>`** for most types of lists
	- **`<DT>`** and **`<DD>`** for glossary lists
- Lists can be embedded


```
<!DOCTYPE html>
<HTML lang="fr">
	<HEAD>
		<TITLE> HTML document </TITLE>
	</HEAD>
	<BODY>
		<p>The devine comedy of Danta consists of 3 books:</p>
		<ul>
			<li>Hell</li>
			<li>Purgatory</li>
			<li>Heaven</li>
		</ul>
		<br>
		<ol>
			<li>Hell</li>
			<li>Purgatory</li>
			<li>Heaven</li>
		</ol>
	</BODY>
</HTML>
```
***No list inside paragraphs (```<p>```)***


<iframe src="https://codesandbox.io/embed/1462kkkqll?fontsize=14&view=editor" title="Ex1" style="width:100%; height:600px; border:0; border-radius: 4px; overflow:hidden;" sandbox="allow-modals allow-forms allow-popups allow-scripts allow-same-origin"></iframe>


### Accents in HTML

- The saving of an HTML document on the server uses **ASCII**
	- More space for accented characters
- We then use key words
	- Examples :
		- à : **`&agrave;`** , &#224
		- ç : **`&ccdil;`** , &#231
		- é : **`&eacute;`** , &#233
		- è : **`&egrave;`** , &#232
		- ê : **`&ecirc;`** , &#234


## Accents in HTML

- Put  **`<meta charset="utf-8">`** inside **`<head>`**

```
<!DOCTYPE html>
<HTML lang="fr">
	<HEAD>
		<TITLE> HTML document </TITLE>
		<meta charset="utf-8">
	</HEAD>
	<BODY>
		<p>The devine comedy of Danta consists of 3 books:</p>
		...
	</BODY>
</HTML>
```
<iframe src="exemples/bonjour5.html"  class='code'   style='height:200px; width: 100%;'> </iframe>


```
...
<P>Glossary list:</P>
<DL>
     <DT> OLE <DD> Object Linking and Embedding
     <DT> SQL <DD> Structured Query Language
</DL>
```

<iframe src="exemples/bonjour6.html"  class='code'   style='height:200px; width: 100%;'> </iframe>


## Hypertext links

**`<A HREF="../menu.htm">`** | **`retour`** |  **`<\A>`** 
--- || --- || ---
*Opening beacon*       | *Text*   |     *Closing beacon*    
----------     
- The linking beacon is **`<A>`**
- Hypertext links exist in 2 forms
	- absolute
	- relative
- Link to a distant document using its URL

	**`<A HREF="http://www.cern.ch">CERN's home page</A>`**


## Anchors	

- The beacon **`<balise id="toto">`** defines an anchor
- Link is made by **`<a href=”#toto”>`**
- It works with every beacon
- An image can trigger a link


```
...
<H3>Absolut link :</H3>
<A HREF="/Users/pierre-fredericvillard/Desktop/web/bonjour.html">
<H2 ALIGN=CENTER>Return to the first HTML document</H2></A>
<H3>Relative link :</H3>
<A HREF="bonjour2.html">Return to the second document HTML</A>
<P>
<H3>Here are the seconds, third and fourth examples :</H3>
<P>Go directly to the<A HREF="#quatrième">Fourth</A> example!</P>
<UL>
<LI>second example</LI>
<LI><A HREF="bonjour4.html">third </A>exemple</LI>
<LI>...</LI>
<LI>...</LI>
<LI>...</LI>
<LI id="quatrième">you are at the fourth example</LI>
```

<iframe src="exemples/bonjour7.html"  class='code'   style="height:200px; width: 100%" scrolling="yes"> </iframe>


## Images
- Images are inserted as follows**`<IMG SRC="  " ALT=" ">`**
- The exact dimensions of the pictures are displayed
   - &rarr; optimises the loading speed
```
<P><IMG SRC="map.gif" alt="map of the USA"></P>
```

 <iframe src="exemples/bonjour9.html" class='code' style="height:200px; width:600px" scrolling="yes"> </iframe>  


## Character styles

- **`<EM>`** Emphasis (italic text)
- **`<STRONG>`** "Stronger" emphase (usually bold)
- **`<B>`** Bold text
- **`<CITE>`** Italic quote
- **`<I>`** Italic text
- **`<CODE>`** Source code of a file or program
- **`<SAMP>`** Text example 


```
<EM> To highlight </EM>
<STRONG>Even stronger</STRONG>
<P>Put in bold : <B>It's bold</B></P> 
<P>Work is health <CITE> A famous stranger</CITE></P> 
<P> Italic text : <I> Italic </I></P> 
<P><CODE>#include stdio.h</CODE></P> 
<HR>
<ADDRESS>E-mail : toto@toto.fr</ADDRESS>	
```

 <iframe src="exemples/bonjour11.html" class='code' style="height:200px; width:600px" scrolling="yes"> </iframe>


## Arrays

```
<TABLE>
	<TR>
		<TH>Legend</TH>
		<TD>Data</TD>
		<TD>Data</TD>
	</TR></TABLE>
<TABLE>
<CAPTION> ISI First year</CAPTION>
	<TR>
		<TH>Number of students</TH>
		<TD>45</TD>
	</TR>
	<TR>
		<TH>Average year</TH>
		<TD>20</TD>
	</TR>  
</TABLE>
```

<iframe src="exemples/bonjour12.html" class='code' style="height:120px; width:600px" scrolling="yes"> </iframe>


- Array components
	- The caption : describes the array's content
	- The headers : Labels of every series of data 
	- The data : Information displayed in the array 
	- The cells : Boxes of the array
- **`<TABLE>`** Defines a table
- **`<CAPTION>`** Defines the caption
- **`<TR>`** Defines a caption 
- **`<TH>`** Defines headers 
- **`<TD>`** Defines data
- **`<TD></TD>`** Defines an empty cell 


### Complex arrays
```
<TABLE>
	<TR>
		<TH ROWSPAN=2 COLSPAN=2></TH>
		<TH COLSPAN=2>Connexion</TH>
	   <TH ROWSPAN=2>Knight</TH>   </TR>
	<TR>
		<TH>MotherBoard</TH>
		<TH>Control board</TH>   </TR>
	<TR>
		<TH ROWSPAN=2>Hard drive</TH>
		<TD>E-IDE</TD>
		<TD>Figure A</TD>
		<TD>Figure D</TD>
		<TD ROWSPAN=2>Figure G</TD>   </TR>
	<TR>
		<TD>SCSI</TD>
		<TD>Figure B</TD>
		<TD>Figure E</TD>   
	</TR>
</TABLE>
```

<iframe src="exemples/bonjour13.html" class='code' style="height:120px; width:600px" scrolling="yes"> </iframe>


## Forms

- **`<FORM>`** Defines a form

- **`<INPUT>`** Indicates that this is an element from the form

	- **`TYPE`** Attribute : 
		- "**`RADIO`**"(round button)
		- "**`CHECKBOX`**"(checking case)
		- "**`TEXT`**"(character string)

	- The attribute **`NAME`** : states the given name to the element

- **`<INPUT TYPE="SUBMIT" VALUE="VALIDER">`** defines the submission button


```
<form>
	<p>
		Mr. <input name=qualite type=radio value=mr> 
		Mrs <input name=qualite type=radio value=mme> 
		Mlle <input name=qualite type=radio value=mlle>
	</p>
	<p>
		Your last name : <input name=patronyme size=15> 
		Your first name : <input name=prenom size=30>
	</p>	
	<p>
		Your password: <input name=clef type=password size=6> 
	</p>
	<p>
		Number : <input name=no type=int size=5>
		Street <input name=lieu type=radio value=rue checked>
		Place <input name=lieu type=radio value=place>
		Avenue <input name=lieu type=radio value=ave>
		Boulevard <input name=lieu type=radio value=bvd>
	</p>
	<p>
		Address : <input name=adresse type=text size=40>
	</p>
	<p>
		Postal code : <input name=cp type=int size=5> 
		City : <input name=ville size=15>
	</p>
	<p>
		<input type=submit value=Valider> 
		<input type=reset value=Annuler>
	</p>
</form>
```

<iframe src="exemples/PROG15.HTM" class='code' style="height:120px; width:600px" scrolling="yes"> </iframe>


```
<form>
	<p>
		You want a pizza containing:
		<select MULTIPLE name="composants" size="6">
			<option>Tomatoes
			<option selected>Olives
			<option>onions
			<option>Garlic
			<option>Sweet herbs
			<option>Parmesan
			<option>Carp
			<option>Pepper
			<option>Chocolate
		</select>
	</p>
</form>
```

<iframe src="exemples/PROG16.HTM" class='code' style="height:120px; width:600px" scrolling="yes"> </iframe>


## "Multimedia beacons

beacon's name   |   description
-----|----
**`source`**   |  multimedia resource
**`audio`**   |  multimedia resource
**`video`**   |  multimedia resource
**`embed`**   |  multimedia resource  


### Source beacon

- Allows to specify many multimedia content alternatives
- Used with **`<video>`**, **`<audio>`** and **`<picture>`**
- It accepts the following attributes:
	- **`src`** : The URL which contains the resource.
	- **`type`** the kind of media : 
	- Example: 

**`<source type="audio/ogg" src="song.ogg"/>`**


### Audio beacon
- It allows to control audio flow.
- It accepts the following attributes:
	- **`src`** : The URL which contains the resource
	- **`autoplay`** : The reading starts as the opening
	- **`loop`** : The reading is done endlessly
	- **`preload`** : The loading is launched at the opening
	- **`controls`** : Controls will be available
```
<audio controls>
	<source src="horse.ogg" type="audio/ogg">
	<source src="horse.mp3" type="audio/mpeg">
	Your browser does not support the audio element.
</audio>
```

<iframe src="exemples/bonjour14.html" class='code' style="height:50px; width:600px" scrolling="yes"> </iframe>


### Video beacon

- Displays a video without external plugins
- It accepts the following attributes:
	- **`src, autoplay, loop, preload, controls`** 
	- **`audio`** if mute is selected,sound is turned off 
	- **`width`** and **`height`** indicate the size of the video 
	- **`poster`** indicates an image, which portrays the video

```
<video width="640" height="480" preload 
  loop="loop" controls="controls" poster="logo.gif"> 
  <source src="monClip.h264" type="video/h264"/>
  <source src="monClip.mp4" type="video/mp4"/>
</video>
```

<iframe src="exemples/bonjour15.html" class='code' style="height:155px; width:600px" scrolling="yes"> </iframe>


### Watch out for the Browser !

<iframe src="https://en.wikipedia.org/wiki/HTML5_video#Browser_support" class='code' style="height:600px; width:100%" scrolling="yes"> </iframe>