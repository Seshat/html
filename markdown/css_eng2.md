# CSS Programing


### Introduction

- The World Wide Web has introduced cascading style sheets to fill in the HTML language
	- = **Cascading Style Sheets**
- CSS allows managing the layout of the documents 
- Sheets indicate to the HTML beacon their behavior or **style**

<div style="width:10%;  display: block;  margin: auto;">![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/CSS3_logo_and_wordmark.svg/340px-CSS3_logo_and_wordmark.svg.png)</div>


### Styles

- A style regroups every attribut which can be apply to similar kind of texts
	- Attribut: size,format
	- Similar texts: titles, headers, footers
- The styles give a common name to groups of attribut


<iframe src="https://support.office.com/en-ie/article/customize-or-create-new-styles-in-word-d38d6e47-f6fc-48eb-a607-1eb120dec563" class='code' style="height:600px; width:100%" scrolling="yes"> </iframe>


## Writing rules
![](fig/css.png)


### Stylesheet structure

- A stylesheet consists of a whole of styles's description
```
Selectors{
	property_1: values_1;
	property_2: values_2;
}
```
- Example :
```
H2{
	color: navy;
	font : 18px;
	font-family: sans-serif;
}
```


### Clustered selectors 

- Possibility to group up many selectors for a same style description
- Example :
```
	H1, H2, H3, H4 {color: blue}
```
is identical to :
```
	H1 {color: blue}
	H2 {color: blue}
	H3 {color: blue}
	H4 {color: blue} 
```


### Styles's localisation 

- The declaration of style's rules might be either:
	1. **external** to the HTML document in a stylesheet 
	+ **internal** to the HTML document in the section **`HEAD`**


### Internal stylesheet 

- Separation of the **présentation** of the HTML page,
- **Reduces the size** of the HTML page,
- Identical style for the whole of a website,
- Fast **Evolution** of the website «design».
- Stylesheet **Specific to the media** (Screen's size, printer,...)


## External stylesheet

- An external stylesheet is a text file which usually has a **.css** extension
- The link between the HTML document and the CSS file is carried out in the **`<HEAD>`** of an HTML document
- Example : 
```
<HEAD>
	<TITLE>Stylesheets History</TITLE>
	<link REL="StyleSheet" TYPE="text/css" HREF="../styles.css">
</HEAD>
```


### Explication
```
<HEAD>
	<TITLE>Stylesheets History</TITLE>
	<link REL="StyleSheet" TYPE="text/css" HREF="../styles.css">
</HEAD>
```
- The beacon **`<LINK>`** advertises the browser to establish a link
- **`rel=stylesheet`** specifies that it's an external sheet
- **`type="text/css"`** specifies that it consists of cascading styleheets
- **`href=" ... "`** defines the location of the stylesheet


<div style="display: inline-grid;grid-template-columns: 500px 500px 200px;">
	<div>
		HTML
<code><pre>
```
<!DOCTYPE html>
<HTML lang="fr">
	<HEAD>
		<title>The css</title>
		<LINK
			REL="stylesheet" 
			TYPE="text/css"
			HREF="style1.css">		
		<meta charset="utf-8">	
	</HEAD>
	<BODY>
		<H1>A title </H1>
		<H2>Another title </H2>
		<p>A cute section </p>
	</BODY>
</HTML>	
```
</pre>
</code>	</div> 
	<div>
		CSS
<code><pre>
```
body {
	color: red ;
}
h2 { 
	text-align:right;
}
h1 {
	font-family:"Courier";
} 	
p {
	border-style:solid;
}
```
</pre></code>
	</div> 
</div> 

<iframe src="exemples/exemple0.html"  class='code' style='resize: both;overflow: auto;height:150px; width: 100%'> </iframe>


## Internal stylesheet

- An internal stylesheet is inserted as a header of the HTML file via the **`<STYLE>`** element
- This styles will be only defined for the current document

### Example
<div style="display: inline-grid;grid-template-columns: 500px 500px 200px;">
	<div>
<code><pre>
```
<!DOCTYPE html>
<HTML lang="fr">
	<HEAD>
		<title>The css</title>
		<style>
			body {
				color: red ;
			}
			h2 { 
				text-align:right;
			}
			h1 {
				font-family:"Courier";
			} 	
			p {
				border-style:solid;
			}
		</style>
		<meta charset="utf-8">	
	</HEAD>
	<BODY>
		<H1> A titleUn titre </H1>
		<H2> Another title </H2>
		<p> A cute section </p>
	</BODY>
</HTML>	
```
</pre>
</code>	</div> 
	<div>
<iframe src="exemples/exemple0.html"  class='code' style='resize: both;overflow: auto;height:500px; width: 300px'> </iframe>
	</div> 
</div> 


## Styles

<iframe src="https://www.tutorialrepublic.com/css-reference/css3-properties.php" style='height:600px'>


## The classes

- Must start by a "**.**"
- Defines properties for many selectors
- Is called using "**`class=`**" as a beacon parameter
- Example
	- In the css
```
.monStyle { 
  background-color: yellow;
  text-align: center;
}
```
	- In the html
```
<p class="monStyle"> A text </p>
```


### Example
<div style="display: inline-grid;grid-template-columns: 500px 500px 200px;">
	<div>
		HTML
<code><pre>
```
<H1 class="joli"> A title </H1>
<H2> An <b class="joli">another</b> title </H2>
<p> A cute <b>section</b></p>
<p class="joli"> A cute section</p>	
<ul>
	<li>Toto</li>
	<li class="joli">Tata</li>
	<li>Titi</li>
</ul>	
```
</pre>
</code>	</div> 
	<div>
		CSS
<code><pre>
```
.joli { 
  color: red;
  font-family: Gill Sans Extrabold, sans-serif;
  text-decoration: underline;
}
```
</pre></code>
	</div> 
</div> 

<iframe src="exemples/exemple1.html"  class='code' style='resize: both;overflow: auto;height:225px; width: 100%'> </iframe>


## ID

- Must start by a "**#**"
- Defines properties of a **unique** element
- Is called using "**`id=`**" as a beacon parameter
- Example
	- In the css
```
#uniqueStyle { 
	   background-color: yellow;
	   text-align: center;
}
```
	- In the html
```
<p id="monStyle"> A text </p>
```


### Example
<div style="display: inline-grid;grid-template-columns: 500px 500px 200px;">
	<div>
		HTML
<code><pre>
```
<H1> A title </H1>
<H2> An <b>other</b> title </H2>
<p> A cute <b>section</b></p>
<p> A cute section</p>	
<ul>
	<li>Toto</li>
	<li id="tresJoli">Tata</li>
	<li>Titi</li>
</ul>	
```
</pre>
</code>	</div> 
	<div>
		CSS
<code><pre>
```
h1{	
  text-decoration: underline; }
b{
  color: red; }
.joli { 
  font-weight: bold;
  font-family: 'Helvetica Neue', sans-serif;
  color: blue;
}
```
</pre></code>
	</div> 
</div> 

<iframe src="exemples/exemple2.html"  class='code' style='resize: both;overflow: auto;height:225px; width: 100%'> </iframe>


## The div beacon
- The **`<div>`** beacon is a block
- It's a rectangular object
- Can be allocated on many lines
- Owns the following attributs **`margin`**, **`padding`**, **`width`**, **`height`**
- **Usually defined by a Class**
```
<div class="xxx">
	<H1> ... </H1>
	<H2> ... </H2>
	<p> ... </p>
	<p> ... </p>	
	<p> ... </p>			
</div>
```


### Example
<div style="display: inline-grid;grid-template-columns: 500px 500px 200px;">
	<div>
		HTML
<code><pre>
```
<div class="partie1">
	<p> A cute <b>section</b></p>
	<p> A cute section</p>
</div>	
<div class="partie2">
	<ul>
		<li>Toto</li>
		<li>Tata</li>
	</ul>	
</div>
```
</pre>
</code>	</div> 
	<div>
		CSS
<code><pre>
```
.partie1{	
  border:4px solid red;
  border-radius: 10px;
  margin:15px;
}
.partie2 { 
  border:4px dotted black;
  padding:5px;
}
```
</pre></code>
	</div> 
</div> 

<iframe src="exemples/exemple3.html"  class='code' style='resize: both;overflow: auto;height:175px; width: 100%'> </iframe>


## The span beacon
- The **`<span>`** beacon is inline
- Registers in the content flow 
- No height or width or margin
- Placed inside a section 
- **Usually defined by a class**

```
<p><span class="xxx">0000000<span class="xxx">111111</span>
222222</span></p>
```


### Example
<div style="display: inline-grid;grid-template-columns: 550px 450px 200px;">
	<div>
		HTML
<code><pre>
```
<p> 
  The <span class="joli1">span beacon
  </span> is <span class="joli2">inline
  </span>, registers in the <span 
  class="joli2">content flow</span>, 
  can be allocated on <span class=
  "joli2">many lines</span>. 
</p>
```
</pre></code>	
	</div> 
	<div>
		CSS
<code><pre>
```
.joli1{	
  color: red;
  font-weight: bold;
}
.joli2 { 
  color: blue;
  font-style: italic;
}
```
</pre></code>
	</div> 
</div> 

<iframe src="exemples/exemple4.html"  class='code' style='resize: both;overflow: auto;height:175px; width: 100%'> </iframe>


## W3C validation
- CSS validation can be realised <a href="http://jigsaw.w3.org/css-validator/"  target="_blank">ici</a>
- HTML validation can be realised <a href="https://validator.w3.org"  target="_blank">ici</a>

<div style="display: inline-grid;grid-template-columns: 500px 500px 200px;">
	<div>
		![](fig/fig7.png)
	</div>
	<div>
		![](fig/fig8.png)
	</div>
</div>


## Margins

![](fig/fig2.png)
- margin = external margin 
- padding = internal margin

	- ***Note*** : Modify the internal margin (**padding**) would result to grow up the concerned box, however modify the external margin (**margin**) has no impact of the box's dimensions


### How to test your own style
- By using the **Lorem Ipsum**
- A fake test used in printing
- Standard since the years **1500**
- Comes from "De Finibus Bonorum et Malorum" from **Cicéron**

![](fig/fig3.png)   |   ![](fig/fig4.png)
--- | ---


## Lorem Ipsum
<div style="height:500px; 
	background: #FFFFFF;
	border:4px #000000 solid; 
	border-radius: 10px;
	box-shadow: 5px 10px;
	overflow:scroll;">
	<pre style="white-space: pre-wrap"><code>
<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non lacus pretium, mattis enim efficitur, tempus ex. Nam facilisis in orci nec tincidunt. Ut molestie id nibh eu imperdiet. Proin tellus ex, vestibulum nec lorem quis, viverra sollicitudin justo. Mauris vitae enim dapibus, vulputate massa eget, facilisis elit. Nullam dapibus porta ultrices. Nam pellentesque ligula nec orci consectetur imperdiet. Nullam vehicula dapibus mi. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>

<p>Etiam et tortor gravida, maximus ex eu, convallis tortor. Ut scelerisque enim vel metus mattis aliquam. Pellentesque dictum laoreet velit, vel pulvinar ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus sit amet interdum massa. Donec sagittis ipsum ante, et molestie arcu finibus non. Mauris facilisis at eros vitae vestibulum. Duis ultricies et ex sit amet sagittis.</p>

<p>Integer interdum, nibh a tristique porta, nisl turpis faucibus mauris, in lobortis enim ligula at tellus. Sed a lacus massa. Morbi dapibus, tellus eu fringilla ultrices, tortor mauris blandit erat, at fermentum diam leo at lorem. Pellentesque egestas risus et ex imperdiet, commodo pulvinar lectus sollicitudin. Nulla sit amet vulputate velit. Vivamus ac eros tincidunt, rhoncus justo at, volutpat dui. Vivamus a bibendum metus. Nunc vulputate ipsum in arcu accumsan, ut rhoncus ante venenatis. Aenean sem ex, ullamcorper a eleifend vitae, scelerisque non sapien. Ut porttitor cursus leo, pulvinar aliquet augue euismod vel. Aenean fermentum auctor tortor, rutrum varius purus venenatis et. Ut condimentum commodo libero at tristique. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec quam dolor, ultrices eu ex in, eleifend imperdiet purus. Proin aliquam bibendum ante, ut consectetur justo tincidunt et.</p>

<p>Etiam in sem eu lorem dictum ultrices a non erat. Nullam vitae tincidunt massa. Cras in ex mauris. Mauris pharetra risus ac metus cursus, at vulputate odio scelerisque. Vivamus eu tristique dui. In dolor orci, lacinia non lectus vel, luctus sollicitudin nunc. Quisque tortor arcu, iaculis sit amet egestas nec, rutrum porttitor nulla. Sed mattis gravida sem, sit amet sollicitudin nunc malesuada scelerisque. Nullam rhoncus metus eu lacus pretium gravida. Nulla vestibulum sapien sem, vel ultricies neque pulvinar vel. Curabitur tincidunt lobortis nunc, et viverra nisl laoreet in.</p>

<p>Suspendisse ultricies ornare imperdiet. Nullam semper rhoncus ultrices. Phasellus mollis molestie lorem. Praesent volutpat ligula id urna luctus congue. Quisque in posuere velit, eu pellentesque massa. Vivamus convallis, erat dignissim porttitor gravida, odio orci feugiat massa, ut tincidunt lectus est ac mauris. Aliquam bibendum, dolor vitae commodo porta, risus purus ultrices urna, in cursus elit magna at magna.</p>
</code></pre>
</div>


### Example

<iframe src="exemples/exemple5.html"  class='code' style='resize: both;overflow: auto;height:500px; width: 100%'> </iframe>


<div style="display: inline-grid;grid-template-columns: 400px 440px 200px;">
	<div>
![](fig/fig5.png)
	</div>
	<div>
		***Help :***
		<ul>
<li>margin: <span style="color:blue">50px</span> <span style="color:green">100px</span> ;
<li>padding-right: <span style="color:green">200px</span> ;
<li>text-align: right ;
		</ul>	
	</div>
</div>

![](fig/fig6.png)


## Boxes management
- **`float`** To automatically positionning the elements
	- &rarr; **`left`** or **`right`**) 
- **`margin: 0 auto;`** to center a element
- **`div`** with class allowing properties 

```
.boites{
	border-radius: 10px;
	float:left;	
	background-color: #FFAAAA;
	border:4px solid red	
}
```


### Automatically positionning
<iframe src="exemples/boite1.html" style="width: 100%; height: 500px;"></iframe>

- &rarr; Depends on the **size** of the boxes and the webpage


### Example with structured beacon
![](fig/fig9.png)


### Small improvement:
- Another police
- Background color
- A little bit of padding 
- Curved edges

<div style="display: inline-grid;grid-template-columns: 500px 500px 200px;">
	<div>
		<iframe src="exemples/exemple6.html" style="width: 100%; height: 500px;"></iframe>
	</div>
	<div>
		<code><pre style="white-space: pre-wrap">
```
font-family: 'Trebuchet MS', Arial, sans-serif;	
border-radius: 10px 40px 10px 40px; 	border:1px solid black;
padding: 5px;
box-shadow: 20px 20px black;
```
		</pre></pre>
	</div>
</div>


### A few inovations with CSS3
- Curved edges
- Shadow
- Gradient
- Multiple background images
- Multi-column layout
- Transformation
- Transition

&rarr; Cf <a target="_blank" href="https://medium.com/beginners-guide-to-mobile-web-development/whats-new-in-css-3-dcd7fa6122e1">Documentation</a>


## Flexible boxes
- Boxes must be put in a container (**`body`** or a **`div`**)
- This container must have 
	- **`display: flex;`**
	- A <a target="_blank" href="https://www.w3schools.com/css/css3_flexbox.asp">display</a> property
- The elements in the contener can also have a <a target="_blank" href="https://www.w3schools.com/css/css3_flexbox.asp">property</a>
- We can have flex boses in other flex boxes
<div style="display: inline-grid;grid-template-columns: 500px 500px 200px;">
	<div>
		<code><pre>
			```
.flex-container {
  display: flex;
  height: 300px;
  justify-content: center;
  align-items: center;
}
```
		</pre></code>
	</div>
	<div>
		<code><pre>
			```
.flex-element {
	order: 1;
}		
```
		</pre></code>
	</div>		 


### Example
<iframe src="exemples/boite2.html" style="width: 100%; height: 500px;"></iframe>	


## Grid display
- Boxes can be put in a container (**`body`** or a **`div`**)
- This container must have
	- **`display: grid;`**
	- A <a target="_blank" href="https://www.w3schools.com/css/css_grid.asp">display</a> property
- Elements in the container can also have <a target="_blank" href="https://www.w3schools.com/css/css_grid.asp">properties</a>

<div style="display: inline-grid;grid-template-columns: 500px 500px 200px;">
	<div>
		<code><pre>
			```
.grid-container {
  display: grid;
  grid-column-gap: 50px;
  grid-template-columns: 200px 200px 200px;
}
```
		</pre></code>
	</div>
	<div>
		<code><pre>
			```
.grid-element {
  grid-row-start: 1;
  grid-row-end: 3;
}		
```
		</pre></code>
	</div>		 


### Example
<iframe src="exemples/boite3.html" style="width: 100%; height: 500px;"></iframe>	